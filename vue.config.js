module.exports = {
  publicPath: process.env.NODE_ENV === 'production'
    ? '/~cs62160007/learn_boostrap/'
    : '/'
}
